import React from "react";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import {
  authenticateUser,
  logoutUser
} from "../../state-management/authentication/authenticationActions";
import LoginPage from "./LoginPage";

class Login extends React.Component {
  _authenticate = () => {
    this.props.login({
      callbackUrl: `${window.location.origin}/login`,
      stateHash: new Date().getTime()
    });
  };

  _logout = () => {
    this.props.logout();
  };

  _renderAuthenticationState = () => {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    } else if (this.props.authenticationInProgress) {
      return <h2>Authentication in progress</h2>;
    } else return <LoginPage _authenticate={this._authenticate} />;
  };

  render() {
    return this._renderAuthenticationState();
  }
}

const mapStateToProps = ({ authentication, user: { name, avatarUrl } }) => ({
  isAuthenticated: authentication.isAuthenticated,
  name,
  avatarUrl
});

const mapDispatchToProps = dispatch => ({
  login: ({ oAuthProvider, callbackUrl, stateHash }) =>
    dispatch(
      authenticateUser({
        oAuthProvider,
        callbackUrl,
        stateHash
      })
    ),
  logout: () => dispatch(logoutUser())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
